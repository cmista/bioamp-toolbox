classdef direct_port < handle
%
% This class implements the direct access to the port. 
%
% methods available:
% - discover_ports: discover all the port available
% - close_all_serial_port: close all serial port availables
% Christian Mista, 2017

    properties(Access=private)
        %serial port
        com_port;
    end 
    
    methods(Access=public)
        
        %discover open serial ports 
        function ports = discover_ports(direct_port)
            ports = instrhwinfo('serial');
        end
        %close all serial ports
        function close_all_serial_port(direct_port, OPENBCI_PORT)
            direct_port.com_port = OPENBCI_PORT;
            close_existing_openbci(OPENBCI_PORT);
        end
        
        function ports = port_ocupied(direct_port)
            ports = direct_port.com_port;
        end
    end
end