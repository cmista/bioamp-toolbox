classdef t_openbci_interface < handle
%
% This object implements an openBCI interface to read data from the serial port.
%
% It is meant to be used with OpenBCI 32bit board "chipkit". With this hardware,
% the dongle gets the data from the board and broadcast it on the serial 
% port: COM8, using the FTDI driver. 
%
% This interface handles the basic communications required to extract data
% from the device:
%
% - t_openbci_interface: constructor
% - open_port: open the com port
% - reset_hardware: reset the hardware and check the validity of the status message
% - start_streaming: sets the device in streaming mode
% - stop_streaming: sets the device in not streaming mode
% - get_bytes_available: returns the number of bytes in the buffer
% - get_eeg_packets: reads available packets and return the eeg samples of all channels
% - flush_buffer: flushes the serial buffer entirely
% - status_hardware: return status of the system
% - turn_on_off_channel: turn off the channel selected
% - send_command: send a command to the device
% - channels_status: return channels status
%
% Reference: http://docs.openbci.com/software/02-OpenBCI_Streaming_Data_Format
%
% Created by Christian Mista, 2017 (based on Frederic Simard, 2015)
%
    properties(Access=private)
        
        %open bci serial port
        com_port;
        
        %channels struct contain all the parameter of each channel
        channels = struct('Number',{},...
                 'Status',{},...
                 'Gain',{},...
                 'Input',{},...
                 'Bias',{},...
                 'SBR2',{}...
             ); 
    end
    
    methods(Access=public)
        
        % Initialise the open bci com port
        function openbci_interface = t_openbci_interface(OPENBCI_PORT)
            %load constant values
            openbci_constants;
            % configure the port
            openbci_interface.com_port = serial(OPENBCI_PORT,'BaudRate',OPENBCI_BAUDRATE);
        end
        
        % Open the port
        function open_port(openbci_interface)
            fopen(openbci_interface.com_port)
        end
        
        % stop streaming and close the port
        function close_port(openbci_interface)
            % stop streaming
            fwrite(openbci_interface.com_port,STOP_STREAMING_MSG,'uchar');
            % close
            fclose(openbci_interface.com_port);
        end
        
        % Reset the hardware and validate the status message
        function [message_text] = reset_hardware(openbci_interface)
            openbci_constants;
                       
            % send a reset command
            fwrite(openbci_interface.com_port,RESET_MSG,'uchar');
            message_text = {'Reset device: writing v ASCII code'};
            
            % read the status data
            status_chunk = fread(openbci_interface.com_port,STATUS_PACKET_LENGTH);
            status_chunk1 = fread(openbci_interface.com_port,STATUS_PACKET_LENGTH);
            
            % check device status
            if ~isempty(status_chunk)
                if ~strfind(char(status_chunk1)','$$$')
                    message_text = [message_text, {'OpenBCI: Reset didn''t recovered or msg not framed correctly\n'}];
                    fprintf('OpenBCI: Reset didn''t recovered or msg not framed correctly\n');
                    return;
                end
            else
                    message_text = [message_text, {'OpenBCI: Reset didn''t recovered, check Hardware\n'}];
                    fprintf('OpenBCI: Reset didn''t recovered, check Hardware\n');
                return;
            end
            
            % indicate the user that the port opened correctly
            %fprintf('OpenBCI: port opening and reset proceeded correctly\n');
            message_text = [message_text, {'OpenBCI: port opening and reset proceeded correctly'}];
                                
            % flush the buffer
            flushinput(openbci_interface.com_port);
        end
        
        % sends a "start streaming" command
        function start_streaming(openbci_interface)
            openbci_constants;
            % go in streaming mode
            fwrite(openbci_interface.com_port,START_STREAMING_MSG,'uchar');
            pause(0.01);
            % flush the buffer
            flushinput(openbci_interface.com_port);
        end
        
        % sends a "stop streaming" command and flush the buffer
        function stop_streaming(openbci_interface)
            openbci_constants;
            % stop streaming
            fwrite(openbci_interface.com_port,STOP_STREAMING_MSG,'uchar');
            % wait for the command to be effective
            pause(0.01);
            % flush the buffer
            flushinput(openbci_interface.com_port);
        end
        
        
        % - turn_off_channel: turn on/off the channel selected
        function turn_on_off_channel(openbci_interface,channel)
            % turn on/off channel
            fwrite(openbci_interface.com_port,char(channel),'uchar');
            % wait for the command to be effective
            pause(0.005);
        end
        
        % - send_command: send a command to the device
        function send_command(openbci_interface,command)
            % turn off channel
            fwrite(openbci_interface.com_port,char(command),'uchar');
            % wait for the command to be effective
            pause(0.005);
            %discard response from the BIOAMP
            openbci_interface.flush_buffer();
        end
             
        % sends a "status" command and flush the buffer
        function message_text = status_hardware(openbci_interface)
            openbci_constants;
            message_text ={'send a status menssage ? ASCII code'};
            % status MSG
            fwrite(openbci_interface.com_port,STATUS_MSG,'uchar');
            % wait for the command to be effective
            pause(0.01);
            % read the status data
            status_read = fread(openbci_interface.com_port,STATUS_PACKET_LENGTH);
            % wait for the command to be effective
            pause(0.01);
            %remove null ASCII characters from data
            status_read(status_read==0) = [];
            %joint message
            message_text = [message_text,{char(status_read(1:125))'},...
                                         {char(status_read(126:236))'},...
                                         {char(status_read(237:345))'},...
                                         {char(status_read(346:end))'},...
                                         {'OpenBCI: status read correctly'}];
            
            %Read status CHSET registers                         
            indx=strfind(char(status_read)' , 'SET: 0x');
            
            %Read index status Bias register
            indx_bias=strfind(char(status_read)' , 'BIAS_SENSN: 0x');
            bias_channels_first_half = hexToBinaryVector((char(status_read(indx_bias+14))'),4);
            bias_channels_second_half = hexToBinaryVector((char(status_read(indx_bias+15))'),4);
            bias_channels = [bias_channels_first_half bias_channels_second_half];

            for i = 1:length(indx) 
                %read status and gain
                status_gain_channel = hexToBinaryVector((char(status_read(indx(i)+7))'),4);
                %set number of channel
                openbci_interface.channels(i).Number = strcat('channel',num2str(i));
                %store if channel om/off
                openbci_interface.channels(i).Status = status_gain_channel(1);
                %store gain
                openbci_interface.channels(i).Gain = bi2de(status_gain_channel(2:4),'left-msb');
                %SBR2 and Mux register
                SBR2_MUX_channel = hexToBinaryVector((char(status_read(indx(i)+8))'),4);
                %store channel SBR2
                openbci_interface.channels(i).SBR2 = SBR2_MUX_channel(1);
                %store gain
                openbci_interface.channels(i).Input = bi2de(SBR2_MUX_channel(2:4),'left-msb');
                %store bias
                openbci_interface.channels(9-i).Bias =  bias_channels(i);
            end
            
            % flush the buffer
            flushinput(openbci_interface.com_port);
        end

        % Check how many packets are in the buffer
        function [nb_packets] = get_nb_packets_available(openbci_interface)
            openbci_constants;
            nb_packets = floor(openbci_interface.com_port.BytesAvailable/DATA_PACKET_LENGTH);
        end
        
        % Get the packets from the buffer, extract eeg samples and returns them
        function [eeg_data, nb_packets] = get_eeg_packets(openbci_interface)
            openbci_constants;
            nb_packets = floor(openbci_interface.com_port.BytesAvailable/DATA_PACKET_LENGTH);
            if nb_packets>0
                [ eeg_data ] = unpack_openbci_eeg(fread(openbci_interface.com_port,DATA_PACKET_LENGTH*nb_packets),nb_packets,NB_CHANNELS, DATA_PACKET_LENGTH, PACKET_FIRST_WORD);
            end
        end
        
        % flush the buffer and start anew
        function flush_buffer(openbci_interface)
            flushinput(openbci_interface.com_port);
        end
        
        function channels = channels_status(openbci_interface)
            channels = openbci_interface.channels;
        end
    end
    
end

