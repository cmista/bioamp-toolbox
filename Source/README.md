*******************************************************************************************************
The BioAmp toolbox is a MATLAB toolbox based on Abstraction Layers and OOP. This GUI facilitates the control and configuration 
of the BioAmp amplifier.


To run the GUI in MATLAB
>> BioAmp

To run the configuration panel in MATLAB
>> configuration_BioAmp

To run the Impedance panel
>> Impedance (TBI)


Laboratorio de Prototipado electronico e impresion 3D - 
Facultad de Ingenieria - 
Universidad Nacional de Entre Rios
*******************************************************************************************************

Layer diagram

-------------------------------------------------------------------------------------------
|																						  |
|										BioAmp											  |
|  																						  |
-------------------------------------------------------------------------------------------
|											|						|					  |
|		configuration_BioAmp        		|	Impedance(TBI)      | Data_collection(TBI)|
|											|						|					  |	
-------------------------------------------------------------------------------------------
|								|														  |
|								|														  |
|								|														  |	
|	class direct_port			|				class t_openbci							  |	
|								|														  |
|								|														  |
|								|----------------------------------------------------------
|								|														  |	
|								|			class t_openbci_interface					  |
|								|														  |
|								|														  |
|								|				-------------------------------------------
|								|				|						|				  |
|								|				|	openbci_constants	|	unpack_eeg	  |	
|								|				|						|				  |
|								|				|						|				  |
-------------------------------------------------------------------------------------------
|																						  |
|																						  |
|								Hardware												  |
|																						  |
|																						  |
-------------------------------------------------------------------------------------------

function BioAmp: is the launcher of the application and coordinates the different functions

function configuration_BioAmp: connects and disconnects from port, create the t_openbci instance 
for the communication, and facilitates the configuration of each channel. 

class direct_port: read serial port information before t_openbci class is instanced.

class t_openbci: high level communication with the device.

class t_openbci_interface: low level communication with the device.

function Impedance: receive a t_openbci instance from BioAmp, so read the status of the channels 
and modify the connection in order to quantify the electrode Impedance. 



*******************************************************************************************************
Christian Mista, 2017
README created using Notepad++

New feature...?
cmista(at)bioingenieria.edu.ar








								