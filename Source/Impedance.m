function varargout = Impedance(varargin)
% IMPEDANCE MATLAB code for Impedance.fig
%      IMPEDANCE, by itself, creates a new IMPEDANCE or raises the existing
%      singleton*.
%
%      H = IMPEDANCE returns the handle to a new IMPEDANCE or the handle to
%      the existing singleton*.
%
%      IMPEDANCE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IMPEDANCE.M with the given input arguments.
%
%      IMPEDANCE('Property','Value',...) creates a new IMPEDANCE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Impedance_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Impedance_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Impedance

% Last Modified by GUIDE v2.5 28-Apr-2017 15:49:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Impedance_OpeningFcn, ...
                   'gui_OutputFcn',  @Impedance_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Impedance is made visible.
function Impedance_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Impedance (see VARARGIN)

% Choose default command line output for Impedance
handles.output = hObject;

    %initializate all the plots                
    for i=1:8
       subplot(1,8,i); 
       %handles for the plots
       impedance_plots(i) = bar(0);
       %remove Xtick labels 
       set(gca,'XTickLabel',[])
       %set the impedance range from 0 to 10k 
       set(gca,'YLim',[0 10000])
       %label each bar with the corresponding channel 
       xlabel(['Channel ' num2str(i)],'FontSize',12) 
    end
    
    %store the handles of the plots
    handles.Impedance = impedance_plots;
    
    %find main GUI handle 
    handles.BioAmp = findobj('Tag', 'BioAmp');
 
    %create timer to update the impedance graph
    handles.timer = timer(...
         'ExecutionMode', 'fixedSpacing', ...    % Run timer repeatedly.
         'Period', 0.02, ...                        % Initial period is 0.5 sec.
         'TimerFcn', {@update_display,handles}); % Specify callback function.
    

    
% Update handles structure
guidata(hObject, handles);
   
   %start timer 
    start(handles.timer);
% UIWAIT makes Impedance wait for user response (see UIRESUME)
% uiwait(handles.Impedance);

%update display graph
function update_display(~, ~, handles)

    %Return the status of the BioAmp channels to check the impedance
    BioAmp_config = BioAmp('read_configuration',handles);
    
    %make a copy in case it change later 
    [Dummy channels_status] = BioAmp_config.status;
    
    %Shut-Down all channels
    BioAmp_config.openbci_turn_off()
    
    %code turn-on for channels 
    code_on  = [33 64 35 36 37 94 38 42];
    code_off = [49 50 51 52 53 54 55 56];
    
    %Loop for iterate across the channels to calculate impedance
    for channel=1:8
        %turn on by channel to measure impedance
        if(~channels_status(channel).Status)
            %see channel on
            %channels_status(channel).Number
            %turn on the channels individually
            BioAmp_config.openbci_turn_on_off_channel(code_on(channel))
            %implementation of P measure of impedance
            % corriente sinusoidal de 6 nA - 31.2Hz al canal.
            
            %word for impedance measure connect testing signal
            Word = [ 'z'              ...
                      num2str(channel)...
                      num2str(1)      ...
                      num2str(0)      ...
                      'Z'             ...
                ];
            
            %Send the impedance command connecion
            BioAmp_config.openbci_command(Word);
            
            %....impedance measurement TBI here....
            %
            %
                %test data for debbuging
                data = randn(1,1)*1000;
                set(handles.Impedance(channel),'YData', abs(data))
            
            %word for disconnect testing signal
            Word = [ 'z'              ...
                      num2str(channel)...
                      num2str(0)      ...
                      num2str(0)      ...
                      'Z'             ...
                ];
            
            %Send the impedance command connecion
            BioAmp_config.openbci_command(Word);
            
            %turn_off channel
            BioAmp_config.openbci_turn_on_off_channel(code_off(channel))
        end
    end
    
    %restore channels 
    for channel=1:8
        if(~channels_status(channel).Status)
            BioAmp_config.openbci_turn_on_off_channel(code_on(channel))
        end
    end
    

    

% --- Outputs from this function are returned to the command line.
function varargout = Impedance_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close Impedance.
function Impedance_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Impedance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    %before exit stop and delete timer
    stop(handles.timer)
    delete(handles.timer)

% Hint: delete(hObject) closes the figure
delete(hObject);
