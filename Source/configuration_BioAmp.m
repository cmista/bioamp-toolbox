function varargout = configuration_BioAmp(varargin)
% CONFIGURATION_BIOAMP MATLAB code for configuration_BioAmp.fig
%      CONFIGURATION_BIOAMP, by itself, creates a new CONFIGURATION_BIOAMP or raises the existing
%      singleton*.
%
%      H = CONFIGURATION_BIOAMP returns the handle to a new CONFIGURATION_BIOAMP or the handle to
%      the existing singleton*.
%
%      CONFIGURATION_BIOAMP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CONFIGURATION_BIOAMP.M with the given input arguments.
%
%      CONFIGURATION_BIOAMP('Property','Value',...) creates a new CONFIGURATION_BIOAMP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before configuration_BioAmp_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to configuration_BioAmp_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help configuration_BioAmp

% Last Modified by Christian Mista 03-Apr-2017 15:29:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @configuration_BioAmp_OpeningFcn, ...
                   'gui_OutputFcn',  @configuration_BioAmp_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before configuration_BioAmp is made visible.
function configuration_BioAmp_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to configuration_BioAmp (see VARARGIN)

% Choose default command line output for configuration_BioAmp
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

%clear variables and Command Window
%clear all
%clc

% UIWAIT makes configuration_BioAmp wait for user response (see UIRESUME)
% uiwait(handles.Configuration);


% --- Outputs from this function are returned to the command line.
function varargout = configuration_BioAmp_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
    if isfield(handles, 'openbci')
        varargout{2} = handles.openbci;
    else
        varargout{2} = 0;
    end 


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1
    if(get(hObject,'Value')==1)
        terminal_output(handles,'Channel 1 is on'); 
        handles.openbci.openbci_turn_on_off_channel(33);
    else
        handles.openbci.openbci_turn_on_off_channel(49);
        terminal_output(handles,'Channel 1 is off');
    end
    %refresh of the channel in the GUI
    refresh(hObject, eventdata, handles);    
    
% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2
   if(get(hObject,'Value')==1)
        terminal_output(handles,'Channel 2 is on'); 
        handles.openbci.openbci_turn_on_off_channel(64);
    else
        terminal_output(handles,'Channel 2 is off');
        handles.openbci.openbci_turn_on_off_channel(50);
   end
   %refresh of the channel in the GUI
   refresh(hObject, eventdata, handles); 

% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3
    if(get(hObject,'Value')==1)
        terminal_output(handles,'Channel 3 is on'); 
        handles.openbci.openbci_turn_on_off_channel(35);
    else
        terminal_output(handles,'Channel 3 is off');
        handles.openbci.openbci_turn_on_off_channel(51);
    end
    %refresh of the channel in the GUI
    refresh(hObject, eventdata, handles);

% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4
    if(get(hObject,'Value')==1)
        terminal_output(handles,'Channel 4 is on'); 
        handles.openbci.openbci_turn_on_off_channel(36);
    else
        terminal_output(handles,'Channel 4 is off');
        handles.openbci.openbci_turn_on_off_channel(52);
    end
    %refresh of the channel in the GUI
    refresh(hObject, eventdata, handles);
        
% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox5
    if(get(hObject,'Value')==1)
        terminal_output(handles,'Channel 5 is on'); 
        handles.openbci.openbci_turn_on_off_channel(37);
    else
        terminal_output(handles,'Channel 5 is off');
        handles.openbci.openbci_turn_on_off_channel(53);
    end
    %refresh of the channel in the GUI
    refresh(hObject, eventdata, handles);

% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox6
    if(get(hObject,'Value')==1)
        terminal_output(handles,'Channel 6 is on'); 
        handles.openbci.openbci_turn_on_off_channel(94);
    else
        terminal_output(handles,'Channel 6 is off');
        handles.openbci.openbci_turn_on_off_channel(54);
    end
    %refresh of the channel in the GUI
    refresh(hObject, eventdata, handles);


% --- Executes on button press in checkbox7.
function checkbox7_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox7
    if(get(hObject,'Value')==1)
        terminal_output(handles,'Channel 7 is on'); 
        handles.openbci.openbci_turn_on_off_channel(38);
    else
        terminal_output(handles,'Channel 7 is off');
        handles.openbci.openbci_turn_on_off_channel(55);
    end
    %refresh of the channel in the GUI
    refresh(hObject, eventdata, handles);
        
% --- Executes on button press in checkbox8.
function checkbox8_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox8
    if(get(hObject,'Value')==1)
        terminal_output(handles,'Channel 8 is on');
        handles.openbci.openbci_turn_on_off_channel(42);
    else
        terminal_output(handles,'Channel 8 is off');
        handles.openbci.openbci_turn_on_off_channel(56);
    end
    %refresh of the channel in the GUI
    refresh(hObject, eventdata, handles);


% --- Executes on button press in Connect_Btn.
function Connect_Btn_Callback(hObject, eventdata, handles)
% hObject    handle to Connect_Btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    %Direct control of serial ports 
    port = direct_port();
    %Discover ports
    serialInfo = port.discover_ports;
    
    terminal_output(handles,'port available: ');
    if(isempty(serialInfo.SerialPorts)==0)
        terminal_output(handles,char(serialInfo.SerialPorts));
    else
        terminal_output(handles,'No com port available');
        return;
    end
    
    %close any open comunication 
    port.close_all_serial_port(serialInfo.SerialPorts);
    
    %changing lable in the popupmenu
    set(handles.popupmenu1, 'String', serialInfo.SerialPorts );
    
    %conecting to the port available 
    terminal_output(handles, 'connecting to the port available...');
       
    % Create a new openbci class and connect to available port 
    openbci = t_openbci(serialInfo.SerialPorts);
    
    if(isvalid(openbci))
        %inform correct connection
        terminal_output(handles,'Port open correctly')
    end;
    
    %store the class instance in the guidata 
    handles.openbci = openbci;
    %store the port connection 
    handles.port = port;
    guidata(hObject, handles);

    %refresh of the channel in the GUI
    Status_Btn_Callback(hObject, eventdata, handles)


    
    
    
    
    
    
% --- Add a new line in the terminal.
function terminal_output(handles, text)
% handles    structure with handles and user data (see GUIDATA)
% text       text to be add in the listbox

    %Max amount of lines 
    Max_size_lines = 15;
    
    %Read text in the listbox
    text_listbox = get(handles.terminal, 'String');
    
    %check maximum lines condition
    if (size(text_listbox) < Max_size_lines)
        %add new line if max line is not reached
        text_listbox = [text_listbox ;{text}];
        set(handles.terminal,'String', text_listbox)
    else
        %remove first line and add a new one if maximum line is reached
        new_text_listbox = cell(size(text_listbox));
        new_text_listbox(1:end-1)=text_listbox(2:end);
        new_text_listbox(end)={text};
        set(handles.terminal,'String',new_text_listbox)
    end
    %draw now
    drawnow
        

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function terminal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to terminal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Reset_Btn.
function Reset_Btn_Callback(hObject, eventdata, handles)
% hObject    handle to Reset_Btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    %Reset and receive message
    Message = handles.openbci.openbci_reset();
    
    %show stored message
    for i = 1:size(Message')
        terminal_output(handles,Message{i})
    end
    
    %refresh of the channel in the GUI
    refresh(hObject, eventdata, handles);


% --- Executes on button press in Refresh_Btn.
function Status_Btn_Callback(hObject, eventdata, handles)
% hObject    handle to Status_Btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    %Read status of channels
    [Message, channels] = handles.openbci.status();
    
    %set the checkbox of the channels
    set(handles.checkbox1,'Value',~channels(1).Status)
    set(handles.checkbox2,'Value',~channels(2).Status)
    set(handles.checkbox3,'Value',~channels(3).Status)
    set(handles.checkbox4,'Value',~channels(4).Status)
    set(handles.checkbox5,'Value',~channels(5).Status)
    set(handles.checkbox6,'Value',~channels(6).Status)
    set(handles.checkbox7,'Value',~channels(7).Status)
    set(handles.checkbox8,'Value',~channels(8).Status)
    
    %string assing to the popup menu
    popupmenu_str = {};
    %update channels on in the popup menu
    for ii = 1:length(channels)
        %if channel is on 
        if(~channels(ii).Status)
            %show in the popup menu
            popupmenu_str = [popupmenu_str {channels(ii).Number}];
        end
    end
    %set string popup menu
    set(handles.channel_popmenu,'String',popupmenu_str);
    
    %store the channels
    handles.channels = channels;
    guidata(hObject, handles);
    
    
    %show first item available
    if(length(popupmenu_str))
        %Update the GUI data
        channel_popmenu_Callback(hObject, eventdata, handles);
    else
        %warning no channel available .. this will no render the popup menu
        terminal_output(handles,'No channel available');
    end
    
    %show change in terminal    
    for i = 1:size(Message')
        terminal_output(handles,Message{i})
    end

%refresh function to update the GUI    
function refresh(hObject, eventdata, handles)
    if(isfield(handles,'channels'))
        Status_Btn_Callback(hObject, eventdata, handles);
    end


% --- Executes on button press in freq1. 250 Hz
function freq1_Callback(hObject, eventdata, handles)
% hObject    handle to freq1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of freq1
    %Button logic
    set(hObject,'Value',1);
    set(handles.freq2,'Value',0);
    set(handles.freq3,'Value',0);
    set(handles.freq4,'Value',0);
    terminal_output(handles,'250 Hz sample frequency all channels');
    %send command
    handles.openbci.openbci_command(71);

% --- Executes on button press in freq2. 500 Hz
function freq2_Callback(hObject, eventdata, handles)
% hObject    handle to freq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of freq2
    %Button logic
    set(hObject,'Value',1);
    set(handles.freq1,'Value',0);
    set(handles.freq3,'Value',0);
    set(handles.freq4,'Value',0);
    terminal_output(handles,'500 Hz sample frequency all channels');
    %send command
    handles.openbci.openbci_command(70);

% --- Executes on button press in freq3. 1 KHz 
function freq3_Callback(hObject, eventdata, handles)
% hObject    handle to freq3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of freq3
    %Button logic
    set(hObject,'Value',1);
    set(handles.freq1,'Value',0);
    set(handles.freq2,'Value',0);
    set(handles.freq4,'Value',0);
    terminal_output(handles,'1000 Hz sample frequency all channels');
    %send command
    handles.openbci.openbci_command(83);

% --- Executes on button press in freq4. 2 KHz
function freq4_Callback(hObject, eventdata, handles)
% hObject    handle to freq4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of freq4
    %Button logic
    set(hObject,'Value',1);
    set(handles.freq1,'Value',0);
    set(handles.freq2,'Value',0);
    set(handles.freq3,'Value',0);
    terminal_output(handles,'2000 Hz sample frequency all channels');
    %send command
    handles.openbci.openbci_command(65);
    
% --- Executes during object creation, after setting all properties.
function freq4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    
    %set on by default in the 2Khz sample frequency
    set(hObject,'Value',1);


% --- Executes during object creation, after setting all properties.
function checkbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    %enable by default
    set(hObject,'Value',1);

% --- Executes during object creation, after setting all properties.
function checkbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    %enable by default
    set(hObject,'Value',1);


% --- Executes during object creation, after setting all properties.
function checkbox3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    %enable by default
    set(hObject,'Value',1);


% --- Executes during object creation, after setting all properties.
function checkbox4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    %enable by default
    set(hObject,'Value',1);


% --- Executes during object creation, after setting all properties.
function checkbox5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    %enable by default
    set(hObject,'Value',1);


% --- Executes during object creation, after setting all properties.
function checkbox6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    %enable by default
    set(hObject,'Value',1);


% --- Executes during object creation, after setting all properties.
function checkbox8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to checkbox8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    %enable by default
    set(hObject,'Value',1);


% --- Executes during object creation, after setting all properties.
function checkbox7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to checkbox7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    %enable by default
    set(hObject,'Value',1);


% --- Executes on selection change in channel_popmenu.
function channel_popmenu_Callback(hObject, eventdata, handles)
% hObject    handle to channel_popmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns channel_popmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from channel_popmenu
    %read the option selected
    options = get(handles.channel_popmenu,'String');
    selected = get(handles.channel_popmenu,'Value');
    %Search channel selected    
    channel_available = 1;
    while(~strcmp(handles.channels(channel_available).Number,options(selected)))
        channel_available = channel_available + 1;
    end
    
    %handle to the radio buttons
    gain_buttons = [handles.r0...
                    handles.r1...
                    handles.r2...
                    handles.r3...
                    handles.r4...
                    handles.r5...
                    handles.r6];
    %change radio button to select the gain per channel            
    set(handles.GainPanel,'SelectedObject',gain_buttons(handles.channels(channel_available).Gain)+1)
    
    %set input per channel
    set(handles.input_selection,'Value',handles.channels(channel_available).Input+1)
    
    %handle of bias
    Bias_handles = [handles.B0 handles.B1];
    set(handles.Bias,'SelectedObject',Bias_handles(handles.channels(channel_available).Bias+1))
    
    %handle of SBR2
    SBR2_handles = [handles.S0 handles.S1];
    set(handles.SBR2,'SelectedObject',SBR2_handles(handles.channels(channel_available).SBR2+1))
    
    %check input selected is test signal
    if( handles.channels(channel_available).Input == 5)  
        set(handles.input_test,'Visible','on')
    else
        set(handles.input_test,'Visible','off')
    end
   
    
% --- Executes during object creation, after setting all properties.
function channel_popmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to channel_popmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in input_selection.
function input_selection_Callback(hObject, eventdata, handles)
% hObject    handle to input_selection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns input_selection contents as cell array
%        contents{get(hObject,'Value')} returns selected item from input_selection
    %select the input for each channel  
    %read the current channel 
    options = get(handles.channel_popmenu,'String');
    selected = get(handles.channel_popmenu,'Value');
    
    %Search channel selected    
    channel_available = 1;
    while(~strcmp(handles.channels(channel_available).Number,options(selected)))
        channel_available = channel_available + 1;
    end
    
    handles.channels(channel_available).Input = get(hObject,'Value')-1;
    Option_input = get(hObject, 'string');
    %inform in terminal the change 
    message = char(strcat(options(selected),' input is set to: ',...
              Option_input(get(hObject,'Value'))));
    terminal_output(handles, message);
    
    %-------Combine the word and send the command
    %x [CANAL, APAGADO, 
    %   GANANCIA, CONFIGURACION DEL TIPO DE ENTRADA, CONFIGURAION DE
    %   BIAS, CONFIGURACION DE SRB1, CONFIGURACION DE SRB2] X
  
    Word = [ 'x' num2str(channel_available)                     ...
             num2str(handles.channels(channel_available).Status)...
             num2str(handles.channels(channel_available).Gain) ...
             num2str(handles.channels(channel_available).Input) ...
             num2str(handles.channels(channel_available).Bias)  ...
             '0'                                                ...
             num2str(handles.channels(channel_available).SBR2)  ...
             'X']
    
    if( handles.channels(channel_available).Input == 5)  
        set(handles.input_test,'Visible','on')
    else
        set(handles.input_test,'Visible','off')
    end
         
    %update GUI data     
    guidata(hObject,handles);
    %send command     
    handles.openbci.openbci_command(Word)



% --- Executes during object creation, after setting all properties.
function input_selection_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input_selection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
    %set all the input string available
    set(hObject,'String',...
        [{'Normal electrode input'}...
        {'Input shorted (for offset or noise measurements)'}...
        {'BIAS measurement'}...
        {'MVDD for supply measuremen'}...
        {'Temperature sensor'}...
        {'Test signal'}...
        {'BIAS_DRP'}...
        {'BIAS_DRN'}...
        ]);




% --- Executes on button press in Disconnect_Btn.
function Disconnect_Btn_Callback(hObject, eventdata, handles)
% hObject    handle to Disconnect_Btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    %disconnect from any port available
    handles.port.close_all_serial_port(handles.port.port_ocupied());
    %clear the port string popupmenu 
    set(handles.popupmenu1, 'String', 'No Port' );
    %Inform in the terminal
    terminal_output(handles,'--- Port close ---');
    


% --- Executes on key press with focus on Configuration or any of its controls.
function Configuration_WindowKeyPressFcn(hObject, eventdata, handles)
% hObject    handle to Configuration (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

        switch eventdata.Key
            %pressing r refresh
            case 'r'
                Status_Btn_Callback(hObject, eventdata, handles);
                
        end


% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function Configuration_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to Configuration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when selected object is changed in GainPanel.
function GainPanel_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in GainPanel 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
    %Combine the word to send to the channel configuration of the gain
    %read the option selected
    options = get(handles.channel_popmenu,'String');
    selected = get(handles.channel_popmenu,'Value');
    %Search channel selected    
    channel_available = 1;
    while(~strcmp(handles.channels(channel_available).Number,options(selected)))
        channel_available = channel_available + 1;
    end
    
    %inform in terminal the change 
    message = char(strcat(options(selected),' gain is set to: ',...
        get(get(handles.GainPanel,'SelectedObject'), 'string')));
    terminal_output(handles, message);
    
    
    gain_selected = get(get(handles.GainPanel,'SelectedObject'), 'tag');
    handles.channels(channel_available).Gain = str2num(gain_selected(2)); 
    
    %-------Combine the word and send the command
    %x [CANAL, APAGADO, 
    %   GANANCIA, CONFIGURACION DEL TIPO DE ENTRADA, CONFIGURAION DE
    %   BIAS, CONFIGURACION DE SRB1, CONFIGURACION DE SRB2] X
  
    Word = [ 'x' num2str(channel_available)                     ...
             num2str(handles.channels(channel_available).Status)...
             num2str(handles.channels(channel_available).Gain)  ...   
             num2str(handles.channels(channel_available).Input) ...
             num2str(handles.channels(channel_available).Bias)  ...
             '0'                                                ...
             num2str(handles.channels(channel_available).SBR2)  ...
             'X']
         
    %update GUI data     
    guidata(hObject,handles);
    %send command     
    handles.openbci.openbci_command(Word)      
         


% --- Executes when selected object is changed in Bias.
function Bias_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in Bias 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
    %Combine the word to send to the channel configuration BIAS
    %read the option selected
    options = get(handles.channel_popmenu,'String');
    selected = get(handles.channel_popmenu,'Value');
    %Search channel selected    
    channel_available = 1;
    while(~strcmp(handles.channels(channel_available).Number,options(selected)))
        channel_available = channel_available + 1;
    end
    
    %inform in terminal the change 
    if(strcmp(get(get(handles.Bias,'SelectedObject'), 'string'),'yes'))
        message = char(strcat(options(selected),' BIAS is connected '));
    else
        message = char(strcat(options(selected),' BIAS diconnected '));
    end
    
    %inform in the terminal
    terminal_output(handles, message);
    
    
    BIAS_connected = get(get(handles.Bias,'SelectedObject'), 'tag');
    handles.channels(channel_available).Bias = str2num(BIAS_connected(2)); 
    
    %-------Combine the word and send the command
    %x [CANAL, APAGADO, 
    %   GANANCIA, CONFIGURACION DEL TIPO DE ENTRADA, CONFIGURAION DE
    %   BIAS, CONFIGURACION DE SRB1, CONFIGURACION DE SRB2] X
  
    Word = [ 'x' num2str(channel_available)                     ...
             num2str(handles.channels(channel_available).Status)...
             num2str(handles.channels(channel_available).Gain)  ...   
             num2str(handles.channels(channel_available).Input) ...
             num2str(handles.channels(channel_available).Bias)  ...
             '0'                                                ...
             num2str(handles.channels(channel_available).SBR2)  ...
             'X']
         
    %update GUI data     
    guidata(hObject,handles);
    %send command     
    handles.openbci.openbci_command(Word)      


% --- Executes when selected object is changed in SBR2.
function SBR2_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in SBR2 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
    %Combine the word to send to the channel configuration BIAS
    %read the option selected
    options = get(handles.channel_popmenu,'String');
    selected = get(handles.channel_popmenu,'Value');
    %Search channel selected    
    channel_available = 1;
    while(~strcmp(handles.channels(channel_available).Number,options(selected)))
        channel_available = channel_available + 1;
    end
    
    %inform in terminal the change 
    if(strcmp(get(get(handles.SBR2,'SelectedObject'), 'string'),'yes'))
        message = char(strcat(options(selected),' SBR2 is connected '));
    else
        message = char(strcat(options(selected),' SBR2 diconnected '));
    end
    
    %inform in the terminal
    terminal_output(handles, message);
    
    
    SBR2_connected = get(get(handles.SBR2,'SelectedObject'), 'tag');
    handles.channels(channel_available).SBR2 = str2num(SBR2_connected(2)); 
    
    %-------combine the word and send the command
    %x [CANAL, APAGADO, 
    %   GANANCIA, CONFIGURACION DEL TIPO DE ENTRADA, CONFIGURAION DE
    %   BIAS, CONFIGURACION DE SRB1, CONFIGURACION DE SRB2] X
  
    Word = [ 'x' num2str(channel_available)                     ...
             num2str(handles.channels(channel_available).Status)...
             num2str(handles.channels(channel_available).Gain)  ...   
             num2str(handles.channels(channel_available).Input) ...
             num2str(handles.channels(channel_available).Bias)  ...
             '0'                                                ...
             num2str(handles.channels(channel_available).SBR2)  ...
             'X']
         
    %update GUI data     
    guidata(hObject,handles);
    %send command     
    handles.openbci.openbci_command(Word)     


% --- Executes on selection change in input_test.
function input_test_Callback(hObject, eventdata, handles)
% hObject    handle to input_test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns input_test contents as cell array
%        contents{get(hObject,'Value')} returns selected item from input_test
    %change the input connection per channel
    switch(get(hObject, 'Value'))
        case 1
            handles.openbci.openbci_command(48) 
            terminal_output(handles,'Test signal: Connected to GND');
        case 2
            handles.openbci.openbci_command(45)            
            terminal_output(handles,'Test signal: Connected to square �1.875mV, 0.97Hz');
        case 3
            handles.openbci.openbci_command(61)
            terminal_output(handles,'Test signal: Connected to square �1.875mV, 1.95Hz');
        case 4
            handles.openbci.openbci_command(112)            
            terminal_output(handles,'Test signal: Connected to DC');
        case 5
            handles.openbci.openbci_command(91)            
            terminal_output(handles,'Test signal: Connected to square �3.75mV, 0.97Hz');
        case 6
            handles.openbci.openbci_command(93)            
            terminal_output(handles,'Test signal: Connected to square �3.75mV, 1.95Hz');
        otherwise 
         error('Incorrect Input test signal')   
    end



% --- Executes during object creation, after setting all properties.
function input_test_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input_test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
    %initialization of the input selection popup menu
    set(hObject, 'Visible', 'off');
    set(hObject, 'String', [{'Conecta a GND'} ...
                             {'Conecta a la se�al de testeo �1.875mV, 0.97Hz'}  ...
                             {'Conecta a la se�al de testeo �1.875mV, 1.95Hz'}  ...
                             {'Conecta a se�al de DC'}                          ...
                             {'Conecta a la se�al de testeo �3.75mV, 0.97Hz'}   ...
                             {'Conecta a la se ?nal de testeo �3.75mV, 1.95Hz'} ...
        ])
    %select the default value
    set(hObject,'Value',5)

    
    
