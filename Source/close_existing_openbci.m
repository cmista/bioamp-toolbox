function close_existing_openbci(OPENBCI_PORT)
            
    % load opened ports
    newobjs = instrfind;

    for ii=1:length(newobjs)
        if strcmp(newobjs(ii).Port,OPENBCI_PORT) && strcmp(newobjs(ii).Status,'open');
            fclose(newobjs(ii));
        end
    end
end

