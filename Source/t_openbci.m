classdef t_openbci < handle
%
% This class implements the openbci object. It provides methods to record
% trials of specific duration to ASCII files.
%
% methods available:
%  - t_openbci: constructor, it initializes the interface
%  - status: check status openbci
%  - openbci_turn_on_off_channel: send command to turn on/off channels
%  - openbci_command: send a command
%  - openbci_reset: reset hardware 
%  - add_data: add a sample to the logger
%  - close_logger: dumps the last data and close the file
%  - openbci_turn_off: turn off all channels
%
% Frederic Simard, Atom Embedded, 2015
%
% Modified by Christian Mista, 2017
%
    
    properties(Access=private)
        
        interface = t_openbci_interface.empty();
        logger = t_openbci_logger.empty();
        
    end
    
    methods(Access=public)
        
        % Object constructor
        function openbci = t_openbci(OPENBCI_PORT)
            
            % initialize the components interface and logger
            openbci.interface = t_openbci_interface(OPENBCI_PORT);
            openbci.logger = t_openbci_logger.empty();
            
            % Open port
            openbci.interface.open_port();             
        end
        
        %turn off channel 
        function openbci_turn_on_off_channel(openbci, channel)
            openbci.interface.turn_on_off_channel(channel);
        end    
        
        %turn off all channel 
        function openbci_turn_off(openbci)
            turn_off_code = [49 50 51 52 53 54 55 56];
            for i=1:8
                openbci.interface.turn_on_off_channel(turn_off_code(i));
            end
        end   
        
        %send command to the device
        function openbci_command(openbci, command)
            openbci.interface.send_command(command);
        end
        
        %send reset hardware to the device
        function [Message] = openbci_reset(openbci)
           Message = openbci.interface.reset_hardware();
        end
        
        % check the status of the device register
        function [Message, channels] = status(openbci)             
            Message = openbci.interface.status_hardware();
            channels = openbci.interface.channels_status();
        end    
        
    end
    
end
