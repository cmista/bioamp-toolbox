function varargout = BioAmp(varargin)
% BIOAMP MATLAB code for BioAmp.fig
%      BIOAMP, by itself, creates a new BIOAMP or raises the existing
%      singleton*.
%
%      H = BIOAMP returns the handle to a new BIOAMP or the handle to
%      the existing singleton*.
%
%      BIOAMP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BIOAMP.M with the given input arguments.
%
%      BIOAMP('Property','Value',...) creates a new BIOAMP or raises the 
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before BioAmp_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to BioAmp_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help BioAmp

% Last Modified by GUIDE v2.5 02-May-2017 11:36:45

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @BioAmp_OpeningFcn, ...
                   'gui_OutputFcn',  @BioAmp_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before BioAmp is made visible.
function BioAmp_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to BioAmp (see VARARGIN)

% Choose default command line output for BioAmp
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

%disable Impedance and record when no connection 
set(handles.Impedance_Btn,'Enable','off'); 
set(handles.Data_collection_Btn,'Enable','off');

imshow('logo.tif')
% UIWAIT makes BioAmp wait for user response (see UIRESUME)
% uiwait(handles.BioAmp);



% --- Outputs from this function are returned to the command line.
function varargout = BioAmp_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Configuration_Btn.
function Configuration_Btn_Callback(hObject, eventdata, handles)
% hObject    handle to Configuration_Btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    %Prevent reopen the configuration panel
    configuration_panel = findobj('Tag','Configuration');
    % if not exists (not empty) not open new
    if isempty(configuration_panel)
         %Open the configuration panel
         handle_configuration_panel = configuration_BioAmp;
         %Store the handel of the configuration panel in the BioAmp GUI
         %data
         setappdata(handles.BioAmp,'hfig',handle_configuration_panel);
    end
    %enable impedance
    set(handles.Impedance_Btn,'Enable','on'); 
    
%Read the object configuration to comunicate to the device outside
%configuration_BioAmp
function BioAmp_config = read_configuration(handles)
        %Read the handel of the configuration panel from BioAmp GUI data    
        handle_configuration_panel = getappdata(handles.BioAmp,'hfig');
        
        [Self_handle BioAmp_config]= configuration_BioAmp('configuration_BioAmp_OutputFcn',...
                                 handle_configuration_panel,0,guidata(handle_configuration_panel));

        


        
% --- Executes on button press in Impedance_Btn.
function Impedance_Btn_Callback(hObject, eventdata, handles)
% hObject    handle to Impedance_Btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

            bioamp = read_configuration(handles);
            if bioamp == 0
                errordlg('Connect to the BioAmp','No connection')
            else
                %continue to the impedance measure
                Impedance;
                %bioamp.status{3}%REMOVE check is working
            end
        

% --- Executes on button press in Data_collection_Btn.
function Data_collection_Btn_Callback(hObject, eventdata, handles)
% hObject    handle to Data_collection_Btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when user attempts to close BioAmp.
function BioAmp_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to BioAmp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);
close all
